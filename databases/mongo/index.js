const mongoose = require("mongoose");
const config = require("../../config/index")
    // Connect to the db
function connectDatabase() {
    mongoose.connect(
        config.dbURL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        },
        function(err, db) {
            if (err) throw err;
            console.log("Connected to database Successfully!");
        },
    );
}

module.exports.connectDatabase = connectDatabase;
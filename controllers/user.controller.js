const { response } = require("express")
const userService = require("../services/user.service.js")

const registerUser = async(req, res) => {
    try {
        const response = await userService.registerUser(req.body)
        res.send(response)
    } catch (err) {
        console.log(err)
        res.status(500).send({ code: 0, message: err.message })
    }
}

const logIn = async(req, res) => {
    try {
        const response = await userService.logIn(req.body)
        res.send(response)
    } catch (err) {
        console.log(err)
        res.status(500).send({ code: 0, message: err.message })
    }
}
const logOut = async(req, res) => {
    try {
        const response = await userService.logOut(req)
        res.send(response)
    } catch (err) {
        console.log(err)
        res.status(500).send({ code: 0, message: err.message })
    }
}

const searchUser = async(req, res) => {
    try {
        const userDetails = await userService.searchUser(req.params['search'])
        res.send(userDetails)
    } catch (err) {
        console.log(err)
        res.status(500).send({ code: 0, message: err.message })
    }
}

module.exports = { registerUser, logIn, logOut, searchUser }
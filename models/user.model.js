const mongoose = require('mongoose');
const Schema = mongoose.Schema,
    bcrypt = require("bcrypt-nodejs");


const UserSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    contactNumber: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    gender: {
        type: String,
        enum: ["male", "female", "other"],
        required: true,
        trim: true
    },
    address: {
        type: String
    },
    country: {
        type: String,
        trim: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        default: '',
        required: true,
    },
    role: [{
        type: String,
        enum: ["admin", "super_admin", "supervisor", "manager", "user", "director", "client"],
        default: 'user'
    }],
    status: {
        type: String,
        enum: ['active', 'inactive', 'deleted', 'locked'],
        default: 'active',
    },
    token: {
        type: Array,
        default: [],
    },

}, {
    timestamps: true
});

//method to decrypt password
UserSchema.methods.validPassword = function(password) {
    var user = this;
    return bcrypt.compareSync(password, user.password);
};

UserSchema.index({ name: "text" });
UserSchema.index({ contactNumber: "text" }, { unique: true });

const User = mongoose.model('User', UserSchema);
module.exports = User;
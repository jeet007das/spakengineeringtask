const { dataValidation, IsMobileNumber } = require("../utils/payloadValidation")
const User = require("../models/user.model"),
    bcrypt = require("bcrypt-nodejs"),
    jwt = require('jsonwebtoken'),
    { secret_key } = require('../config/index'),
    { v4: uuidv4 } = require('uuid');;

const registerUser = async(payload) => {
    const { name = "", contactNumber = "", gender = "", address = "", country = "", password = "" } = payload
    //Here payload validation
    const validate = await dataValidation(payload, ["name", "contactNumber", "gender", "password"])
    if (validate['code']) {
        //Contact Number validation
        if (IsMobileNumber(contactNumber)) {
            // value is ok, use it
            //Here have to check is there any user present with same mobile number in debugger
            const isUserPresent = await User.findOne({ contactNumber })
            if (!isUserPresent) {
                //  const createUser = await User.create({ name, contactNumber, gender, address, country, password })
                let createUser = await new User({ name, contactNumber, gender, address, country, password });

                // becrypt password
                if (password) createUser.password = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);

                // save user
                createUser = await createUser.save();
                if (createUser) return { code: 1, message: "User created successfully" }
                else return { code: 0, message: "User not created" }
            } else return { code: 0, message: `User already present with contactNumber:- ${contactNumber}` }
        } else return { code: 0, message: `Invalid Contact Number.` }
    } else {
        const fieldName = validate['info'].toString()
        return { code: 0, message: `This field is required :- ${fieldName}` }
    }

}

const logIn = async(payload) => {

    console.log("Login controller")
    const { contactNo = "", password = "" } = payload
    //Here payload validation
    const validate = await dataValidation(payload, ["contactNo", "password"])
    if (validate['code']) {
        const isUser = await User.findOne({ contactNumber: contactNo, status: "active" }, { name: 1, contactNumber: 1, password: 1, gender: 1, address: 1, country: 1 })
        console.log(isUser)
            //first check user is present or not
        if (isUser) {
            //Validate password
            if (isUser.validPassword(password)) {
                //for authorized user create Json web token
                const tokenId = uuidv4()
                await User.findOneAndUpdate({ _id: isUser._id }, { $push: { token: tokenId } })
                const web_token = jwt.sign({ name: isUser['name'], contactNumber: isUser['contactNumber'], tokenId }, secret_key, { expiresIn: '1h' });
                const userObj = { name: isUser['name'], contactNumber: isUser['contactNumber'], gender: isUser['gender'], address: isUser['address'], token: web_token }
                return { code: 1, user: userObj, message: "Login successfully", }
            } else return { code: 0, message: "Incorrect Password" }
        } else return { code: 0, message: `User not found with contactNumber:- ${contactNo}` }
    } else {
        const fieldName = validate['info'].toString()
        return { code: 0, message: `This field is required :- ${fieldName}` }
    }

}

const logOut = async(payload) => {
    const { _id, contactNumber = "", tokenId = "" } = payload.user
    const result = await User.updateOne({ _id, contactNumber }, { $pull: { token: tokenId } })
    if (result) {
        return { code: 1, message: "Logout successfully" }
    }

}

const searchUser = async(search_value = "") => {
    if (search_value) {
        let users = await User.find({
            $or: [{
                    "name": {
                        $regex: `^${search_value}`,
                        $options: "-i"
                    }
                },
                {
                    "contactNumber": {
                        $regex: search_value,
                        $options: "i"
                    }
                }
            ]
        }, { name: 1, contactNumber: 1, gender: 1, address: 1, country: 1 }).lean();
        if (users && users.length) {
            return { code: 1, data: users }
        } else return { code: 0, data: [] }
    } else return { code: 0, message: "Search parameter not found." }

}

module.exports = { registerUser, logIn, logOut, searchUser }
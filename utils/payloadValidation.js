module.exports.dataValidation = (payload = {}, fields = []) => {
    if (fields && fields.length) {
        const err_fields = []
        fields.forEach(field => {
            if (!payload[field]) {
                err_fields.push(field)
            }
        })

        if (!err_fields.length) return { code: 1, message: "Data validate successfully." }
        else return { code: 0, info: err_fields }
    }
}

module.exports.IsMobileNumber = (mobileNumber) => {
    var mob = /^[1-9]{1}[0-9]{9}$/;
    // var txtMobile = document.getElementById(txtMobId);
    if (!mob.test(mobileNumber)) {
        return false
    }
    return true;
}
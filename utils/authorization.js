const jwt = require('jsonwebtoken'),
    { secret_key } = require('../config/index'),
    User = require("../models/user.model");
module.exports.gurdian = async(req, res, next) => {
    try {
        const token = req.headers['x-access-token'];
        if (token) {
            const userObj = jwt.verify(token, secret_key)
            if (userObj && Object.keys(userObj).length) {
                const { contactNumber, tokenId } = userObj;
                const user = await User.findOne({ contactNumber, token: { $in: [tokenId] } }, { name: 1, contactNumber: 1, gender: 1, address: 1, country: 1 });
                if (user && Object.keys(user).length) {
                    req.user = JSON.parse(JSON.stringify(user))
                    req.user['tokenId'] = tokenId;
                    next()
                } else {
                    res.send({ code: 0, message: "Unauthorized user." })
                }
            } else {
                res.send({ code: 0, message: "Invalid Token." })
            }
        } else {
            res.send({ code: 0, message: "Token not found." })
        }
    } catch (e) {
        console.log(e)
        res.send({ code: 0, message: e.message });
    }
}
/* eslint-disable new-cap */
const { Router } = require('express');
//import { validator } from '../validations/validator';
const userController = require('../controllers/user.controller')
const { gurdian } = require("../utils/authorization")
    //import { login } from '../validations/admin.validator';
const router = Router()
router.post('/register', userController.registerUser);
router.post('/login', userController.logIn);
//router.delete('/:blogId', auth('verifyToken'), auth('isStaff'),blogController('deleteBlog'));

router.get('/findUser/:search', gurdian, userController.searchUser);
router.get('/logout', gurdian, userController.logOut);


module.exports = router;
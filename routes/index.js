const { userRouter } = require('../routes/user.route');


module.exports.initializeRoutes = (app) => {
    // require('../helpers/passport');
    app.use('/api/user', userRouter);

};

// module.exports = initializeRoutes;
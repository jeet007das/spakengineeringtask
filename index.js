const express = require('express');
const app = express();
const cors = require('cors');
var bodyParser = require('body-parser');
//const jwt = require('./src/common/jwt');
const routes = require('./routes/index');
const { connectDatabase } = require("./databases/mongo/index")
const userRouter = require('./routes/user.route');
const { PORT } = require("./config/index")

//parse data

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());



// allow cross orgin request
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "PUT, DELETE, GET, POST");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});
//connecting mongodb connecting
(async function() {
    try {
        await connectDatabase();
    } catch (error) {
        console.error(error);
        process.exit(1);
    }
})();




// //bind jwt token
// app.use(jwt())

//intializing routes
app.use('/api/user', userRouter);


// start server
const port = process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : PORT;
const server = app.listen(port, async() => {
    console.log('Server listening on port ' + port);
});